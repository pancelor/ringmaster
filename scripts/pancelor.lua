local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

-- todo: mod setting about whether DR is only on pancelor character, or on all chars
event.levelLoad.add("spawnFriend", {order="entities"},function (ev)
  local friend=object.spawn("DeadRingerPhase2",-2,-1,{
    beatDelay={counter=0},
  })
  voice.play(friend, "deadRingerLaugh")
  invincibility.activate(friend, 9999)
end)

customEntities.extend {
  name="pancelor",
  template=customEntities.template.player(0),
  components={
    {
      friendlyName={name="pancelor"},
      initialEquipment={
        items={"ShovelBasic","WeaponEli","Torch1"},
      },
      inventoryCursedSlots={
        slots={weapon=true},
        exemptItems={weaponEli=true},
      },
      bestiary={image="mods/pancelor/bestiary.png"},
      playableCharacter={
        lobbyOrder=1,
      },
      sprite={
        texture="mods/pancelor/images/pance_body.png",
      },
    },
    {
      sprite={
        texture="mods/pancelor/images/pance_heads.png",
      },
    },
  },
}
